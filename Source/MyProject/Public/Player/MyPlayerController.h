// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "../Pawns/MySpectatorPawn.h"
#include "../Pawns/MyCameraComponent.h"
#include "MyPlayerController.generated.h"
/**
 * 
 */
UCLASS()
class MYPROJECT_API AMyPlayerController : public APlayerController
{
	GENERATED_BODY()
	AMyPlayerController(const FObjectInitializer& ObjectInitializer);

public:
	// Begin PlayerController interface
	/** fixed rotation */
	virtual void UpdateRotation(float DeltaTime) override;
	AMySpectatorPawn* GetStrategySpectatorPawn() const;
	UMyCameraComponent* GetSpectatorCameraComponent() const;

};
