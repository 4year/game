// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SpectatorPawn.h"

#include "MyCameraComponent.h"
#include "MySpectatorPawn.generated.h"
/**
 * 
 */
UCLASS()
class MYPROJECT_API AMySpectatorPawn : public ASpectatorPawn
{
	GENERATED_BODY()

private:

	UPROPERTY(Category = CameraActor, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UMyCameraComponent* MyCameraComponent = NULL;

public:
	AMySpectatorPawn(const FObjectInitializer& ObjectInitializer);
	UMyCameraComponent* GetStrategyCameraComponent() const;
};
