// Fill out your copyright notice in the Description page of Project Settings.


#include "../../Public/Player/MyPlayerController.h"


AMyPlayerController::AMyPlayerController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = true;
	SetHidden(false);
	bShowMouseCursor = true;
}


void AMyPlayerController::UpdateRotation(float DeltaTime)
{
	FRotator ViewRotation(0, -60, 0);
	FRotator DeltaRot(0, 0, 0);

	if (PlayerCameraManager)
	{
		PlayerCameraManager->ProcessViewRotation(DeltaTime, ViewRotation, DeltaRot);
	}

	SetControlRotation(ViewRotation);
}

AMySpectatorPawn* AMyPlayerController::GetStrategySpectatorPawn() const
{
	return Cast<AMySpectatorPawn>(GetSpectatorPawn());
}

UMyCameraComponent* AMyPlayerController::GetSpectatorCameraComponent() const
{
	UMyCameraComponent* CameraComponent = NULL;
	UE_LOG(LogTemp, Warning, TEXT("IN GetSpectatorCameraComponent "));


	if (GetStrategySpectatorPawn() != NULL)
	{
		UE_LOG(LogTemp, Warning, TEXT("IN GetSpectatorCameraComponent not null"));

		CameraComponent = GetStrategySpectatorPawn()->GetStrategyCameraComponent();
	}
	return CameraComponent;
}
