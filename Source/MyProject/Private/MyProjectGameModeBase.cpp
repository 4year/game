// Copyright Epic Games, Inc. All Rights Reserved.


#include "MyProjectGameModeBase.h"
#include "Player/MyPlayerController.h"
#include "Pawns/MySpectatorPawn.h"
#include "MyGameStateBase.h"

AMyProjectGameModeBase::AMyProjectGameModeBase(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Screen Message"));

	UE_LOG(LogTemp, Warning, TEXT("IN AMyProjectGameModeBase constructor"));
	PlayerControllerClass = AMyPlayerController::StaticClass();
	SpectatorClass = AMySpectatorPawn::StaticClass();
	DefaultPawnClass = AMySpectatorPawn::StaticClass();
	GameStateClass = AMyGameStateBase::StaticClass();

	if ((GEngine != nullptr) && (GEngine->GameViewport != nullptr))
	{
		GEngine->GameViewport->SetSuppressTransitionMessage(true);
	}
}